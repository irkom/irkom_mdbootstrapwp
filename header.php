<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>IR Blogs</title>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">

    <!-- <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/app.css"> -->
    <!-- Bootstrap core CSS -->
    <link href="<?php bloginfo('template_url'); ?>/css/bootstrap.min.css" rel="stylesheet">

    <!-- Material Design Bootstrap -->
    <link href="<?php bloginfo('template_url'); ?>/css/mdb.min.css" rel="stylesheet">

    <!-- Template styles -->
    <link href="<?php bloginfo('template_url'); ?>/style.css" rel="stylesheet">
    
    <?php  wp_head(); ?> 
  </head>
  <body>
    <!--menu header -->
    <nav class="navbar navbar-toggleable-md navbar-dark fixed-top scrolling-navbar">
        <div class="container">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNav1" aria-controls="navbarNav1" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <img src="<?php bloginfo('template_url'); ?>/img/ir.png" id="brand"  style="width:35px; height:35px; padding-right:5px">             

            <?php
              wp_nav_menu( array(
               'menu'              => 'primary',
               'theme_location'    => 'primary',
               'depth'             => 2,
               'container'         => 'div',
               'container_class'   => 'collapse navbar-collapse',
               'container_id'      => 'navbarNav1',
               'menu_class'        => 'navbar-nav mr-auto',
               'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
               'walker'            => new wp_bootstrap_navwalker())
              );
             ?>
        </div>
    </nav>
    <!--/.Navbar-->
    <!--end menu header -->