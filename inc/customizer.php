<?php
/* 
 * @author: Indra Rahmat
 */

function wpb_customize_register($wp_customize){
    //showcase section
    $wp_customize->add_section('showcase', array(
       'title' => __('Showcase', 'wpbootstrap'),
        'description' => sprintf(__('Option for showcase', 'wpbootstrap')),
        'priority' => 130
    ));
    
    $wp_customize->add_setting('showcase_image1', array(
       'default' => get_bloginfo('template_directory').'/img/showcase.jpg',
        'type' => 'theme_mod'
    ));
    
    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'showcase_image1', array(
        'label' => __('Showcase Image 1', 'wpbootstrap'),
        'section' => 'showcase',
        'settings' => 'showcase_image1',
        'priority' => 1
    )));
    
    $wp_customize->add_setting('showcase_image2', array(
       'default' => get_bloginfo('template_directory').'/img/showcase.jpg',
        'type' => 'theme_mod'
    ));
    
    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'showcase_image2', array(
        'label' => __('Showcase Image 2', 'wpbootstrap'),
        'section' => 'showcase',
        'settings' => 'showcase_image2',
        'priority' => 1
    )));
    
    //heading one
    $wp_customize->add_setting('showcase_heading1', array(
       'default' => _x('Personal Website and Blog', 'wpbootstrap'),
        'type' => 'theme_mod'
    ));
    
    $wp_customize->add_control('showcase_heading1', array(
       'label' => __('Heading 1', 'wpbootstrap'),
        'section' => 'showcase',
        'priority' => 1
    ));
    
    $wp_customize->add_setting('showcase_text1', array(
       'default' => _x('I think if you do something and it turns out pretty good, then you should go do something else wonderful, not dwell on it for too long. Just figure out what’s next.', 'wpbootstrap'),
        'type' => 'theme_mod'
    ));
    
    $wp_customize->add_control('showcase_text1', array(
       'label' => __('Texting 1', 'wpbootstrap'),
        'section' => 'showcase',
            'priority' => 2
    ));
    
    //heading two
    $wp_customize->add_setting('showcase_heading2', array(
       'default' => _x('Personal Website and Blog', 'wpbootstrap'),
        'type' => 'theme_mod'
    ));
    
    $wp_customize->add_control('showcase_heading2', array(
       'label' => __('Heading 2', 'wpbootstrap'),
        'section' => 'showcase',
        'priority' => 3
    ));
    
    $wp_customize->add_setting('showcase_text2', array(
       'default' => _x('I think if you do something and it turns out pretty good, then you should go do something else wonderful, not dwell on it for too long. Just figure out what’s next.', 'wpbootstrap'),
        'type' => 'theme_mod'
    ));
    
    $wp_customize->add_control('showcase_text2', array(
       'label' => __('Texting 2', 'wpbootstrap'),
        'section' => 'showcase',
        'priority' => 4 
    ));
    
    //button
    $wp_customize->add_setting('btn_url', array(
       'default' => _x('#', 'wpbootstrap'),
        'type' => 'theme_mod'
    ));
    
    $wp_customize->add_control('btn_url', array(
       'label' => __('url', 'wpbootstrap'),
        'section' => 'showcase',
        'priority' => 5
    ));
    
    $wp_customize->add_setting('btn_label', array(
       'default' => _x('Blog', 'wpbootstrap'),
        'type' => 'theme_mod'
    ));
    
    $wp_customize->add_control('btn_label', array(
       'label' => __('Btn Label', 'wpbootstrap'),
        'section' => 'showcase',
        'priority' => 6 
    ));
}

add_action('customize_register', 'wpb_customize_register');

