<?php /* Template Name: Blog template */ ?>
<?php get_header(); ?>
<!--end menu header -->
<div class="text-center">
    <div class="blog-header container">
        <blockquote class="blockquote-reverse">
            <p>when you don't create things, you become defined by your tastes rather than ability. your tastes only narrow & exclude people. so create</p>
            <footer>Someone famous in <cite title="Source Title">Why The Lucky Stiff</cite></footer>
        </blockquote>
    </div>
</div>
<div id="stories" class="section-blog">
    <div class="container bs-doc-container">
        <div class="row">
            <div class="col-md-9" role="main">
                <?php if (have_posts()) : ?>
                    <?php while (have_posts()) : the_post(); ?>
                        <?php get_template_part('content', get_post_format()); ?>
                    <?php endwhile; ?>
                    <nav aria-label="Page navigation">
                        <?php wpbeginner_numeric_posts_nav(); ?>
                    </nav>
                <?php else : ?>
                    <p>
                        <?php __('No Posts Found'); ?>
                    </p>
                <?php endif; ?>
            </div>
            <div class="col-md-3" role="complementary">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <?php _e('Categories', 'wpb_theme_setup'); ?>
                    </div>
                    <div class="panel-body text-left">
                        <ul class="list-group">
                            <?php wp_list_categories('sort_column=name&title_li='); ?>
                        </ul>
                    </div>
                </div>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <?php _e('New Post', 'wpb_theme_setup'); ?>
                    </div>
                    <div class="panel-body text-left">
                        <ul class="list-group">
                            <?php wp_get_archives('format=custom&before= <li class="list-group-item">&after=</li>&type=postbypost&limit=100'); ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>

